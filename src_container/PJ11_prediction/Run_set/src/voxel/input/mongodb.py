import argparse
import itertools
import json
import logging
import os.path
import sys

import numpy as np

from pymongo import MongoClient
from gridfs import GridFS
from mrcfile.mrcinterpreter import MrcInterpreter

logger = logging.getLogger(__name__)

argument_parser = argparse.ArgumentParser(add_help=False)

argument_parser.add_argument(
    '--mongodb-uri', '--uri', 
    default='mongodb://localhost:27017/test', 
    help='Specify a resolvable URI connection string for the mongod to which to connect.'
)

def load_data(args):
    db = MongoClient(args.mongodb_uri).get_database()
    fs = GridFS(db, 'mrc')
    for doc in itertools.chain(*zip(
        db.mrc.files.aggregate([
            {'$sample': {'size': db.mrc.files.count() * 0.05}},
            {'$match': {'metadata.label': 0}},
        ]),
        db.mrc.files.aggregate([
            {'$sample': {'size': db.mrc.files.count() * 0.05}},
            {'$match': {'metadata.label': 1}},
        ]),
    )):
        logger.debug(doc)
        mrc = MrcInterpreter(fs.get(doc['_id']))
        data = np.pad(
            mrc.data,
            (
                (max((args.model_depth - mrc.header.nz) // 2, 0), max((args.model_depth - mrc.header.nz - (args.model_depth - mrc.header.nz) // 2), 0)),
                (max((args.model_height - mrc.header.ny) // 2, 0), max((args.model_height - mrc.header.ny - (args.model_height - mrc.header.ny) // 2), 0)),
                (max((args.model_width - mrc.header.nx) // 2, 0), max((args.model_width - mrc.header.nx - (args.model_width - mrc.header.nx) // 2), 0)),
            ),
            mode='constant'
        )[
            -min((args.model_depth - mrc.header.nz) // 2, 0):-min((args.model_depth - mrc.header.nz) // 2, 0) + args.model_depth,
            -min((args.model_height - mrc.header.ny) // 2, 0):-min((args.model_height - mrc.header.ny) // 2, 0) + args.model_height,
            -min((args.model_width - mrc.header.nx) // 2, 0):-min((args.model_width - mrc.header.nx) // 2, 0) + args.model_width,
        ].reshape([args.model_depth, args.model_height, args.model_width, 1])
        label = np.eye(1, args.model_num_classes, doc['metadata']['label'])[0] if 'label' in doc['metadata'] else np.zeros(args.model_num_classes)
        yield data, label
