import argparse
import collections
import csv
import fileinput
import glob
import itertools
import json
import logging
import os.path
import random
import re
import sys

import numpy as np

from mrcfile.mrcinterpreter import MrcInterpreter

logger = logging.getLogger(__name__)

argument_parser = argparse.ArgumentParser(add_help=False)

argument_parser.add_argument(
    'files', 
    nargs='+', 
    help='Specify 09_residueRMSD/*_si.txt or 11_edstats_3DCNN/*_fofc_stats_coor.out style tab-delimited text files.'
)
argument_parser.add_argument(
    '--residue-names', 
    nargs='?', 
    help='Specify comma-separated residue names to load. Default to load all residues.'
)
argument_parser.add_argument(
    '--atom-types', 
    default='C.2,C.3,C.ar,C.cat,N.3,N.am,N.ar,N.pl3,O.2,O.3,O.co2,S.3',
    help='Specify comma-separated atom types to load.'
)
argument_parser.add_argument(
    '--rotation-numbers', '--rotnum',
    default='0',
    help='Specify comma-separated rotation numbers to load.'
)
argument_parser.add_argument(
    '--without-2fofc',
    action='store_true',
    help='Do not use 2fofc map.'
)

def load_data(args):
    for rotnum, (filename, row) in itertools.product(
        args.rotation_numbers.split(','),
        ((fileinput.filename(), row) for row in csv.DictReader(fileinput.input(args.files), delimiter=' ', skipinitialspace=True) if not fileinput.isfirstline()),
    ):
        res_name = row['res_name'] if 'RMSD' in row else row['RT']
        if args.residue_names and res_name not in args.residue_names.split(','):
            continue
        logger.debug((filename, row))
        res_num = row['res_num'] if 'RMSD' in row else row['RN']
        data = []
        for file_path in itertools.chain(
            (
                os.path.join(
                    'data/19_fcmapextend_per_res',
                    re.sub(
                        r'(_si.txt$|_fofc_stats_coor.out$)',
                        '',
                        os.path.basename(filename)
                    ),
                    atom_type + '_' + fg_bg + '_' + res_num + '_fc_cell_rot' + rotnum + '_' + res_num + '.map'
                )
                for atom_type, fg_bg in itertools.product(
                    args.atom_types.split(','),
                    [
                        'fg'
                    ]
                )
            ),
            [] if args.without_2fofc else [os.path.join(
                'data/18_rot_2fofc_map_per_res',
                re.sub(
                    r'(_si.txt$|_fofc_stats_coor.out$)',
                    '',
                    os.path.basename(filename)
                ),
                '2fofc_rot' + rotnum + '_' + res_num + '.map'
            )]
,
        ):
            try:
                mrc = MrcInterpreter(open(file_path, 'rb'))
            except IOError as err:
                logger.warning(err)
                data.append(np.zeros([args.model_depth, args.model_height, args.model_width]))
            else:
                data.append(np.pad(
                    mrc.data,
                    (
                        (max((args.model_depth - mrc.header.nz) // 2, 0), max((args.model_depth - mrc.header.nz - (args.model_depth - mrc.header.nz) // 2), 0)),
                        (max((args.model_height - mrc.header.ny) // 2, 0), max((args.model_height - mrc.header.ny - (args.model_height - mrc.header.ny) // 2), 0)),
                        (max((args.model_width - mrc.header.nx) // 2, 0), max((args.model_width - mrc.header.nx - (args.model_width - mrc.header.nx) // 2), 0)),
                    ),
                    mode='constant'
                )[
                    -min((args.model_depth - mrc.header.nz) // 2, 0):-min((args.model_depth - mrc.header.nz) // 2, 0) + args.model_depth,
                    -min((args.model_height - mrc.header.ny) // 2, 0):-min((args.model_height - mrc.header.ny) // 2, 0) + args.model_height,
                    -min((args.model_width - mrc.header.nx) // 2, 0):-min((args.model_width - mrc.header.nx) // 2, 0) + args.model_width,
                ])
        for (z, y), x in itertools.product([(0, 0), (0, 1), (0, 2), (0, 3), (1, 0), (3, 0)], range(4)):
            yield np.array(
                list(map(
                    lambda T: np.rot90(np.rot90(np.rot90(T, x, axes=(0, 1)), y, axes=(0, 2)), z, axes=(1, 2)),
                    data
                ))
            ).transpose([1, 2, 3, 0]), np.array([float(row['RMSD'] if 'RMSD' in row else row['CCSa'])])
