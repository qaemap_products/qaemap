import argparse
import itertools
import json
import logging
import os.path
import sys

import numpy as np

from mrcfile.mrcinterpreter import MrcInterpreter

logger = logging.getLogger(__name__)

argument_parser = argparse.ArgumentParser(add_help=False)

argument_parser.add_argument(
    '--input-streams', 
    nargs='+', 
    default=(open(f, 'rb') for f in map(str.strip, sys.stdin)), 
    type=argparse.FileType('rb'), 
    help='Specify input files.'
)
argument_parser.add_argument(
    '--input-labels', 
    nargs='+', 
    default=itertools.repeat(None), 
    help='Specify input labels.'
)

def load_data(args):
    for stream, label in zip(args.streams, args.labels):
        logger.debug((stream.name, label))
        mrc = MrcInterpreter(stream)
        data = np.pad(
            mrc.data,
            (
                (max((args.model_depth - mrc.header.nz) // 2, 0), max((args.model_depth - mrc.header.nz - (args.model_depth - mrc.header.nz) // 2), 0)),
                (max((args.model_height - mrc.header.ny) // 2, 0), max((args.model_height - mrc.header.ny - (args.model_height - mrc.header.ny) // 2), 0)),
                (max((args.model_width - mrc.header.nx) // 2, 0), max((args.model_width - mrc.header.nx - (args.model_width - mrc.header.nx) // 2), 0)),
            ),
            mode='constant'
        )[
            -min((args.model_depth - mrc.header.nz) // 2, 0):-min((args.model_depth - mrc.header.nz) // 2, 0) + args.model_depth,
            -min((args.model_height - mrc.header.ny) // 2, 0):-min((args.model_height - mrc.header.ny) // 2, 0) + args.model_height,
            -min((args.model_width - mrc.header.nx) // 2, 0):-min((args.model_width - mrc.header.nx) // 2, 0) + args.model_width,
        ].reshape([args.model_depth, args.model_height, args.model_width, 1])
        label = np.eye(1, args.model_num_classes, int(label))[0] if label is not None else np.zeros(args.model_num_classes)
        yield data, label
