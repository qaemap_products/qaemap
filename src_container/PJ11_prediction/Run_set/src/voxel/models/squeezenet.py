import argparse
import logging

from tensorflow import keras

logger = logging.getLogger(__name__)

argument_parser = argparse.ArgumentParser(add_help=False)

argument_parser.add_argument(
    '--model-num-channels',
    default=16,
    type=int,
    help='Specify the number of input layer channels.'
)
argument_parser.add_argument(
    '--model-num-classes',
    default=1,
    type=int,
    help='Specify the number of output layer classes.'
)
argument_parser.add_argument(
    '--model-width',
    default=24,
    type=int,
    help='Specify the width of input layer.'
)
argument_parser.add_argument(
    '--model-height',
    default=24,
    type=int,
    help='Specify the height of input layer.'
)
argument_parser.add_argument(
    '--model-depth',
    default=24,
    type=int,
    help='Specify the depth of input layer.'
)

def build_graph(input_tensor=None, input_shape=None, training=False, args=None):
    input_layer = keras.layers.Input(tensor=input_tensor, shape=input_shape)
    x = keras.layers.Conv3D(64, 3, strides=(2, 2, 2), padding='same', activation=keras.activations.relu)(input_layer)
    x = fire_moule(x)
    x = fire_moule(x)
    x = keras.layers.MaxPool3D(pool_size=(3, 3, 3), strides=(2, 2, 2))(x)
    x = fire_moule(x, squeeze=32, expand=128)
    x = fire_moule(x, squeeze=32, expand=128)
    x = keras.layers.MaxPool3D(pool_size=(3, 3, 3), strides=(2, 2, 2))(x)
    x = fire_moule(x, squeeze=48, expand=192)
    x = fire_moule(x, squeeze=48, expand=192)
    x = fire_moule(x, squeeze=64, expand=256)
    x = fire_moule(x, squeeze=64, expand=256)
    x = keras.layers.Dropout(0.5)(x, training=training)
    x = keras.layers.Conv3D(args.model_num_classes, 1, activation=keras.activations.linear)(x)
    x = keras.layers.GlobalAveragePooling3D()(x)
    return x

def fire_moule(x, squeeze=16, expand=64):
    x = keras.layers.Conv3D(squeeze, 1, activation=keras.activations.relu)(x)
    x = keras.layers.concatenate(
        [
            keras.layers.Conv3D(expand, 1, activation=keras.activations.relu)(x),
            keras.layers.Conv3D(expand, 3, padding='same', activation=keras.activations.relu)(x)
        ],
        axis=4
    )
    return x
