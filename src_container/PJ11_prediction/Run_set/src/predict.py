#!/usr/bin/env python3

import argparse
import glob
import importlib
import logging
import os
import os.path
import sys

import tensorflow as tf

logger = logging.getLogger(__name__)

argument_parser = argparse.ArgumentParser(
    description=__doc__,
    add_help=False,
)

argument_parser.add_argument(
    '--model-module', '--model_module', '--model', 
    default='voxel.models.lenet5like',
    help='Set the model module name.'
)
argument_parser.add_argument(
    '--input-module', '--input_module', '--input', 
    default='voxel.input.streams',
    help='Set the input module name.'
)
argument_parser.add_argument(
    '--log-level', '--log_level', 
    default=logging.getLevelName(logging.getLogger().level), 
    choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'], 
    help='Set the logger level to the specified level.'
)
argument_parser.add_argument(
    '--logdir', 
    default=sorted(glob.glob('./logs/*'), key=lambda logdir: os.stat(logdir).st_mtime)[-1],
    help='Directory where to read model checkpoints.'
)
argument_parser.add_argument(
    '--max-steps', '--max_steps', 
    default=1,
    type=int,
    help='Number of batches to run.'
)
argument_parser.add_argument(
    '--batch-size', '--batch_size', 
    default=1,
    type=int,
    help='Batch size.'
)
argument_parser.add_argument(
    '--threshould', 
    default=None,
    type=float,
    help='Threshould for prediction.'
)
argument_parser.add_argument(
    '--model-num-channels',
    default=None,
    type=int,
    help='Specify a number of input layer channels.'
)
argument_parser.add_argument(
    '--model-num-classes',
    default=None,
    type=int,
    help='Specify a number of output layer classes.'
)
argument_parser.add_argument(
    '--model-width',
    default=None,
    type=int,
    help='Specify the width of input layer.'
)
argument_parser.add_argument(
    '--model-height',
    default=None,
    type=int,
    help='Specify the height of input layer.'
)
argument_parser.add_argument(
    '--model-depth',
    default=None,
    type=int,
    help='Specify the depth of input layer.'
)

def predict():
    with tf.name_scope('Input'):
        x, y_ = tf.data.Dataset.from_generator(
            lambda: input_module.load_data(args),
            (
                tf.float32,
                tf.float32
            ),
            (
                tf.TensorShape([
                    args.model_depth,
                    args.model_height,
                    args.model_width,
                    args.model_num_channels
                ]),
                tf.TensorShape([
                    1 if args.model_num_classes == 2 else args.model_num_classes
                ])
            )
        ).batch(
            args.batch_size
        ).make_one_shot_iterator(
        ).get_next()

    y = model_module.build_graph(x, training=False, args=args)

    if args.model_num_classes == 1:
        prediction = tf.reshape(tf.reduce_mean(y, 0), [])
    elif args.model_num_classes == 2:
        if args.threshould:
            prediction = tf.reshape(tf.cast(tf.greater(tf.reduce_mean(tf.sigmoid(y), 0), args.threshould), tf.int64), [])
        else:
            prediction = tf.reshape(tf.reduce_mean(tf.sigmoid(y), 0), [])
    else:
        prediction = tf.argmax(tf.reduce_mean(y, 0))

    saver = tf.train.Saver(tf.global_variables())

    with tf.Session() as sess:
        # Restores from checkpoint
        saver.restore(sess, tf.train.latest_checkpoint(
            args.logdir
        ))
        try:
            for step in range(1, args.max_steps + 1):
                logger.info('Step: {0}/{1}'.format(step, args.max_steps))
                print('{prediction}'.format(prediction=prediction.eval()))
                print(end='', flush=True)
        except tf.errors.OutOfRangeError:
            pass

if __name__ == '__main__':
    args, _ = argument_parser.parse_known_args()
    logging.basicConfig(level=args.log_level)
    logger.debug(args)

    model_module = importlib.import_module(args.model_module)

    input_module = importlib.import_module(args.input_module)

    argument_parser = argparse.ArgumentParser(
        parents=[
            argument_parser,
            model_module.argument_parser,
            input_module.argument_parser,
        ],
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        conflict_handler='resolve'
    )
    args = argument_parser.parse_args()
    logger.info(args)

    predict()
