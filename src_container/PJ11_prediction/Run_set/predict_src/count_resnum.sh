#!/bin/bash

if [ $# -lt 1 ];then
	echo "args[0]: prefix list"
	echo "args[1]: pdb file directory"
	exit
fi

pdbdir=$2
sum=0
while read line
do
	pdbfile=`echo ${pdbdir}/${line}.pdb`
	resnum=`cat ${pdbfile}|grep ^ATOM|grep CA|wc -l`
#	echo ${line} ${resnum}
	sum=`expr ${sum} + ${resnum}`
done<$1

echo "${sum}"
