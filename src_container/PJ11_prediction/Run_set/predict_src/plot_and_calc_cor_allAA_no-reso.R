
logfile<-commandArgs(trailingOnly=TRUE)[1]
prefix<-commandArgs(trailingOnly=TRUE)[2]
outname<-commandArgs(trailingOnly=TRUE)[3]
#prefix<-"out_o/train-test_40epoch.sh"

logdata<-read.table(logfile)
colnames(logdata)<-c("jobid","resname","model","objval","boxsize")

pdf(paste(outname,"pdf",sep="."))
for( index in 1:nrow(logdata) ){
    result_file<-paste(prefix,".o",logdata$jobid[index],sep="")
    #cor_file<-paste("cor_",paste(prefix,".dat",sep=""),sep="")
    command<-paste('grep -v -E "^#" ', result_file, '| wc -l')
    size<-system(command,intern=T)
    if(size == 0 ){
        logdata$accval[index]<-NA
        logdata$corval[index]<-NA
    }else{
        result<-read.table(result_file)
        max_val<-max(result)+0.1
       	min_val<-min(result)-0.1
        plot(result,pch=20,col="red",xlab="Label",ylab="Prediction",main=paste(logdata[index,2],logdata[index,3],logdata[index,4],logdata[index,5],sep="_"),xlim=c(min_val,max_val),ylim=c(min_val,max_val))
        #hist(tmp$V2,breaks=seq(min_val,max_val,0.1),main=paste("hist. of prediction in",file,sep=" ") )

        #get R
        logdata$corval[index]<-cor(result$V1,result$V2)
    
        #get accuracy or RMSE
        com<-paste("tail -1",result_file)
        ac_val<-unlist(strsplit(system(com,intern=T),':'))
        logdata$accval[index]<-ac_val[2]
    }
}

write.table(logdata,paste(outname,"dat",sep="."),row.names=F,quote=F,sep="\t")
dev.off()
