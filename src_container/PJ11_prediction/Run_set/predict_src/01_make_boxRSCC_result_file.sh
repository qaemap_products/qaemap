#!/bin/bash

if [ $# -lt 2 ];then
    echo "args[0]: difval.dat file. ex.) ../out_exe_test_20200523_144502_difval.dat"
    echo "args[1]: target name ex.) 5uq1cor_A_target"
    echo "args[2]: box size ex.) 10"
    echo "args[3]: pdb file ex.) ../data/03_refmac/5uq1cor_A_target.pdb"
    echo "args[4]: prefix of output file ex) 5uq1cor_A_target_box10"
    exit
fi

difval_file=$1
tname=$2
bsize=$3
pdb=$4
prefix=$5
cat ${difval_file}|grep ${tname}|grep boxrscc$'\t'${bsize}|sort -k1n > ${prefix}.dat

declare -A brscc
while read line
do
    sp=(`echo -e "${line}"`)
    key=`echo ${sp[0]}_${sp[1]}_${sp[2]}`
    brscc[$key]=`printf '%.2f' ${sp[5]}`
    #echo ${key} ${brscc[${key}]}
done<${prefix}.dat

echo -n > ${prefix}.pdb
while read line
do
    if [[ ${line} =~ ^ATOM ]];then
        res_no=`echo -e "${line}"|cut -c23-26`
        res_name=`echo -e "${line}"|cut -c18-20`
        chain=`echo -e "${line}"|cut -c22`
        key=`echo ${res_no}_${res_name}_${chain}`
        before=`echo -e "${line}"|cut -c-60`
        after=`printf "%6s" ${brscc[$key]}`
        echo -e "${before}${after}" >> ${prefix}.pdb
    ## 結晶構造のBファクターが残ってしまうため、HETATM行はスキップする。
    elif [[ ${line} =~ ^HETATM ]];then
        :
    else
        echo -e "${line}" >> ${prefix}.pdb
    fi
done<${pdb}
