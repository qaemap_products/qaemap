#!/bin/bash

source /usr/local/ccp4/bin/ccp4.setup-sh

if [ $# -lt 3 ];then
	echo "args[0]: pdb file directory"
	echo "args[1]: output directory"
	echo "args[2]: file name  prefix"
	exit
fi

atom_dir=$1
fc_dir=$2
prefix=$3

if [ ! -e ${fc_dir}/${prefix} ];then
	mkdir -p ${fc_dir}/${prefix}
fi

for file in `ls -1 ${atom_dir}/${prefix}/*.pdb`
do
sfall XYZIN ${file} \
      MAPOUT ${fc_dir}/${prefix}/`basename ${file} .pdb`.map <<eof-sfall
title Generate structure factors from coordinates
# Set the mode for structure factor calculation
# using input coords and reflection file
# (NB. It is not compulsory to have HKLIN)
mode ATMMAP
SYMM `grep CRYST1  ${file} | awk '{print $8 $9 $10 $11 $12}'`
#symmetry 19                  
#GRID 204 220 252
end
eof-sfall

done
