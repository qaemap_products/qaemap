#!/usr/bin/perl

use Math::Trig;
use File::Basename;
use strict;

if( $#ARGV < 9 ){
    print "\n";
	print "argv[0]: pdb file (ex. 03_refmac/TRY1_BOVIN_1HJ8_clustalw2_0.80_50_ref.pdb)\n";
	print "argv[1]: output directory of pdb file (ex. 17_rot_refmac)\n";
	print "argv[2]: 2fo-fc file (ex. 04_2fofc_map/TRY1_BOVIN_1HJ8_clustalw2_0.80_50_ref_2fofc.map)\n";
	print "argv[3]: output directory of 2fo-fc data cut out for each residue (ex. 18_rot_2fofc_map_per_res)\n";
	print "argv[4]: prefix of input fc map file (ex. 08_fcmapextend/TRY1_BOVIN_1HJ8_clustalw2_0.80_50)\n";
	print "argv[5]: outout directory of fc map data cut out for each residue (ex. 19_fcmapextend_per_res)\n";
	print "argv[6]: rotation number (ex. rot1)\n";
    print "args[7]: 2fo-fc file of high resolution data (ex. 03_refmac/4i8g_target.mtz)\n";
    print "args[8]: output directory of high resolution 2fofc data cut out for each residue (ex. 11_rot_ac_2fofc_map_per_res)\n";
    print "args[9]: chain ID (ex. A)\n\n";
	exit;
}

my $pdb=$ARGV[0];
my $outpdbdir=$ARGV[1];
my $infofc=$ARGV[2];
my $outfofcdir=$ARGV[3];
my $infc=$ARGV[4];
my $outfcdir=$ARGV[5];
my $rotnum=$ARGV[6];
my $infofcac=$ARGV[7];
my $outfofcacdir=$ARGV[8];
my $objchain=$ARGV[9];
my @rot_mat;
my $outfofc="";
my $outfofcac="";
my $before_id="";
my $index=0;
my $sum_x;
my $sum_y;
my $sum_z;
my $atom_name="";
my $res_no="";
my $resn="";
my $chain="";
my $x="";
my $y="";
my $z="";
my $id="";
my $before_id="";
my $center_x=0;
my $center_y=0;
my $center_z=0;
my @center=(0.0,0.0,0.0);
my $outfc_file="";
my $infc_file="";
my $before_res_no="";
my $before_resn="";
my @box_size;

my $grid_width = $ENV{CELL} / $ENV{GRID};

srand( $rotnum );

if( $rotnum ){
	@rot_mat=&get_rot_matrix( deg2rad( rand(360) ), deg2rad( rand(360) ), deg2rad( rand(360) ) );
}else{
	@rot_mat=&get_rot_matrix;
}

#exe pdbset(rotate pdb file)
mkdir $outpdbdir;
my $outpdb=$outpdbdir . "/" . "rot" . $rotnum . ".pdb";
&exe_pdbset(\@rot_mat,$outpdb,$pdb);

#exe maprot per residue
#calculate center of  haevy atoms of each residue, and execute maprot 
#rotation 2fo-fc map data and cut out for each residue
mkdir $outfofcdir;
$outfofc=$outfofcdir . "/" . "2fofc_rot" . $rotnum;

#for fc per atom type
mkdir $outfcdir;
#for high resolution 2fofc map  per atoms
mkdir $outfofcacdir;
$outfofcac=$outfofcacdir . "/" . "2fofc_rot" . $rotnum;

#read rotated pdb file, calc residue center and cut out map per residue.
open (IN,$outpdb) or die "$!";
while(<IN>){
	next if( $_ !~ /^ATOM/ );
	chomp $_;

	#skip data if atom is hydrogen
	$atom_name=substr( $_ , 12 , 4 );
	next if ( $atom_name =~ /^\s+H/ );

	#get info
	$res_no=substr( $_ , 22 , 4 );
	$resn=substr( $_ , 17 , 3 );
	$chain=substr( $_ , 21 , 1 );
	$x=substr( $_ , 30 , 8 );
	$y=substr( $_ , 38 , 8 );
	$z=substr( $_ , 46 , 8 );
	$id=$res_no . "_" . $chain;

	# Do not cut out the map centered on water molecules.
	next if($resn =~ /HOH/);

    next if($chain ne $objchain );
	if( $before_id ne "" && $id ne $before_id ){
		#get residue center
		$center_x = $sum_x / $index;
		$center_y = $sum_y / $index;
		$center_z = $sum_z / $index;
		$center[0]=sprintf("%8.3f",$center_x / $grid_width);
    	$center[1]=sprintf("%8.3f",$center_y / $grid_width);
		$center[2]=sprintf("%8.3f",$center_z / $grid_width);
		#my $tmin=sprintf("%3d",$res_no-1);
		#print "ATOM    $tmin  CA  VAL A $tmin    $cx$cy$cz  1.00  5.87           C\n";

        #rot and cut out per residue for 2fofc, fc, and high resolution 2fofc
        @box_size=&get_box_size( $center[0], $center[1], $center[2] );
        &exe_rot_and_cut_out(\@box_size,\@rot_mat,$rotnum,$before_res_no,$infofc,$infc,$outfofc,$outfcdir,$infofcac,$outfofcac);

		print STDERR "$before_res_no\t$before_resn\n";

		#initialize
		$sum_x=$x;
		$sum_y=$y;
		$sum_z=$z;
		$index=1;
	}else{
		$sum_x = $sum_x + $x ;
		$sum_y = $sum_y + $y ;
		$sum_z = $sum_z + $z ;
		$index++;
	}
	$before_id=$id;
	$before_res_no = $res_no;
	$before_res_no =~ s/\s*//;
	$before_resn = $resn;
	#print "$res_no---$atom_name---$chain---$x----$y----$z\n";
}
if($before_resn !~ /HOH/){
	#for last residue
	$center_x = $sum_x / $index;
	$center_y = $sum_y / $index;
	$center_z = $sum_z / $index;
	$center[0]=sprintf("%8.3f",$center_x / $grid_width);
	$center[1]=sprintf("%8.3f",$center_y / $grid_width);
	$center[2]=sprintf("%8.3f",$center_z / $grid_width);
	@box_size=&get_box_size( $center[0], $center[1], $center[2] );
	&exe_rot_and_cut_out(\@box_size,\@rot_mat,$rotnum,$before_res_no,$infofc,$infc,$outfofc,$outfcdir,$infofcac,$outfofcac);
	print "*!*!*!*!*!*!*!*!*!*!*!*!*!*!**!*!*!*!*!*!*!*!*!*$chain $objchain $index $before_res_no\n";
	print STDERR "$before_res_no\t$before_resn\n";
}

close(IN);

sub exe_rot_and_cut_out{
    my $box_size=$_[0];
    my $rot_mat=$_[1];
    my $rotnum=$_[2];
    my $before_res_no=$_[3];
    my $infofc=$_[4];
    my $fc=$_[5];
    my $outfofc=$_[6];
    my $outfcdir=$_[7];
    my $infofcac=$_[8];
    my $outfofcac=$_[9];
    my $outfofc_file;
    my $outfc_file;
    my $infc_file;
    my $outfofcac_file; 

    ##for 2fofc
    $outfofc_file=$outfofc . "_" . $before_res_no . ".map";
    if( -e $outfofc_file ){
    	 print "### The file already exist(maprot):$outfofc_file\n";
    }else{
    	&exe_maprot(\@rot_mat,\@box_size,$infofc,$outfofc_file);
    }
    
    #for high resolution 2fofc map
    $outfofcac_file=$outfofcac . "_" . $before_res_no . ".map";
    if( -e $outfofcac_file ){
         print "### The file already exist(maprot):$outfofcac_file\n";
    }else{
        &exe_maprot(\@rot_mat,\@box_size,$infofcac,$outfofcac_file);
    }

    #foreach $infc_file (glob $fc . "/*_" . $before_res_no . "_fc_cell.map"){
    #C_fg_fc_cell.map S_fg_9_fc_cell_rot0_9.map
    foreach $infc_file (glob $fc . "/*_fc_cell.map"){
    	#$outfc_file=$outfcdir . "/" . basename($infc_file, ".map") . "_rot" . $rotnum . "_" . $before_res_no .".map";
        $outfc_file=$outfcdir . "/" . basename($infc_file, "fc_cell.map") . $before_res_no . "_fc_cell_rot" . $rotnum . "_" . $before_res_no .".map";
    	if( -e $outfc_file ){
    		print "### The file already exist(fcrot):$outfc_file\n";
    	}else{
    		print "##exe_maprot($rot_mat[0][0],$box_size[0],$infc_file,$outfc_file);";
    		&exe_maprot(\@rot_mat,\@box_size,$infc_file,$outfc_file);
    	}
    }
}

sub get_box_size{
	my $x=$_[0];
	my $y=$_[1];
	my $z=$_[2];
    my $x_min=0.0;
    my $x_max=0.0;
    my $y_min=0.0;
    my $y_max=0.0;
    my $z_min=0.0;
    my $z_max=0.0;
    my @box_size;

    $x_min=sprintf("%4.0f", $x - 16);
	$x_max = $x_min + 31;
    $y_min=sprintf("%4.0f", $y - 16);
	$y_max = $y_min + 31;
    $z_min=sprintf("%4.0f", $z - 16);
	$z_max = $z_min + 31;

	@box_size=($x_min,$x_max,$y_min,$y_max,$z_min,$z_max);
	return @box_size;
}

sub get_rot_matrix {
	my $alpha=$_[0];
	my $beta=$_[1];
	my $gamma=$_[2];
    my @rot_mat;

	@rot_mat=(
		[ cos($alpha)*cos($beta)*cos($gamma)-sin($alpha)*sin($gamma), -1*cos($alpha)*cos($beta)*sin($gamma)-sin($alpha)*cos($gamma), cos($alpha)*sin($beta) ],
		[ sin($alpha)*cos($beta)*cos($gamma)+cos($alpha)*sin($gamma), -1*sin($alpha)*cos($beta)*sin($gamma)+cos($alpha)*cos($gamma), sin($alpha)*sin($beta) ],
		[ -1*sin($beta)*cos($gamma), sin($beta)*sin($gamma), cos($beta) ]
		);
	return @rot_mat;
}

sub exe_pdbset{
	my $rot_mat=$_[0];
	my $outpdb=$_[1];
    my $pdb=$_[2];

system("pdbset XYZIN $pdb XYZOUT $outpdb <<eof
ROTAte invert matrix    $rot_mat[0][0] $rot_mat[0][1]  $rot_mat[0][2] -
                 $rot_mat[1][0]  $rot_mat[1][1] $rot_mat[1][2] -
                 $rot_mat[2][0]  $rot_mat[2][1] $rot_mat[2][2]
shift  0.00  0.0 0.0
end
eof");
    return $rot_mat;
}

sub exe_maprot{
	my $rot_mat=$_[0];
	my $box_size=$_[1];
	my $in=$_[2];
	my $out=$_[3];

system("maprot mapin $in wrkout  $out << eof
MODE FROM
CELL WORK $ENV{CELL} $ENV{CELL} $ENV{CELL}       90.0 90.0 90.0
GRID WORK $ENV{GRID} $ENV{GRID} $ENV{GRID}
XYZLIM $box_size[0] $box_size[1] $box_size[2] $box_size[3] $box_size[4] $box_size[5]
SYMM WORK 1
AVER
ROTAte matrix    $rot_mat[0][0] $rot_mat[0][1]  $rot_mat[0][2] -
                 $rot_mat[1][0]  $rot_mat[1][1] $rot_mat[1][2] -
                 $rot_mat[2][0]  $rot_mat[2][1] $rot_mat[2][2]
Translate 0.0 0.0 0.0
eof");

}
