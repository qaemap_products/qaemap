#!/bin/bash

#$ -cwd
#$ -pe smp 16
#echo "HOSTNAME=${HOSTNAME}"
#source ~/.bashrc

#  A 2fo - fc map 
#

source /usr/local/ccp4/bin/ccp4.setup-sh

if [ $# -lt 1 ];then
    echo "04_2fofc_map.sh [mtz file list]"
    echo ""
    echo "mtz file list is tab delimited format like below..."
    echo ""
    echo -e "[target mtz]\t[experimentar mtz]"
    echo "03_refmac/1LC0_A_1GCU_A_B99990007_2.4.mtz   03_refmac/1LC0_A_target_1.2.mtz"
    echo ""
    echo ""
    exit
fi

#mtz_dir="03_refmac"
map_dir="04_2fofc_map"

if [ ! -e ${map_dir} ];then
	mkdir ${map_dir}
fi

#for file in `ls -1 ./${mtz_dir}/*.mtz`
while read line
do
    sp=(`echo ${line}`)
#    for file in ${sp[@]}
    for index in `seq 0 1`
    do
        file=`echo ${sp[${index}]}`
        echo $file
        prefix=`basename ${file} .mtz`
    	if [ -e ${map_dir}/${prefix}_2fofc.map ];then
            echo "LOGS * This file already exist: ${map_dir}/${prefix}_2fofc.map"
    		continue
    	fi
        echo "LOGS * Exe fft : ${file}"
fft \
   HKLIN  ${file} \
   MAPOUT  ${map_dir}/${prefix}_2fofc.map \
   << END
#fftspg 19
#GRID 260 280 320
GRID SAMPLE 10
LABIN F1=FWT  PHI=PHWT
end
END
    done
done<$1

