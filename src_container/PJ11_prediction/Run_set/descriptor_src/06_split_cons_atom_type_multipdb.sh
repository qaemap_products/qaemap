#!/bin/bash

if [ $# -lt 2 ];then
	echo "args[0]: pdb file"
	echo "args[1]: output directory"
	exit
fi

pdbfile=$1
outdir=$2

prefix=`basename ${pdbfile} .pdb`

mkdir -p ${outdir}/${prefix}

while IFS=$"\n" read line
do
	if [[ ${line} =~ ^CRYST1 ]];then
		cryst=`echo -e "${line}"`
	fi

	if [[ ${line} =~ ^(ATOM|HETATM) ]];then
		#resSeq=`echo -e "${line}" | cut -c 23-26 | tr -d " "`
		atom_type=`echo -e "${line}" | cut -c 18-20,76-78 | awk -v w_c="${HOH_CHANNEL}" -v l_c="${UNKNOWN_CHANNEL}" '
			{
                gsub(" ", "", $2)
				if($1 ~ /HOH/) {
					print w_c
				}else if($2 ~ /^(C|O|N|S)$/) {
					print $2
				}else{
					print l_c
				}
			}'`
		if [ ! -e ${outdir}/${prefix}/${atom_type}_fg.pdb ];then
			echo -e "${cryst}" > ${outdir}/${prefix}/${atom_type}_fg.pdb
		fi
		echo -e "${line}" >> ${outdir}/${prefix}/${atom_type}_fg.pdb

	fi
done < ${pdbfile}
