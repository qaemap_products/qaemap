import sys

class FileReadLine():
    def __init__(self):
        self.lineData = []

    def ReadLine(self, filename):
        f = open(filename, "r")
        for line in f:
            self.lineData.append(line)

#####################

if __name__ == "__main__":
    args = sys.argv
    argc = len(args)
    if argc != 2:
        print("Usage: python " + args[0] + " filename")
        quit()

    a = FileReadLine()
    a.ReadLine(args[1])

    for line in a.lineData:
        if len(line.split()) != 1:
            print(line, end='')
