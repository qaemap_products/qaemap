#!/bin/bash

for file in `ls -1 03_refmac/*.mtz`
do
    dir=`dirname ${file}`
    mtz=`basename ${file}`
    prefix=`basename ${mtz} .mtz`
    sp=(`echo ${prefix}|tr "_" " "`)
    high_reso_file=`ls -1 ${dir}/${sp[0]}_${sp[1]}_target*.mtz|head -1`
    echo ${dir}/${mtz} ${high_reso_file} ${sp[1]}
done
