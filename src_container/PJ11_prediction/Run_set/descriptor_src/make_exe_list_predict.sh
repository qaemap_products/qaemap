#!/bin/bash

if [ $# -ne 2 ]; then
  echo "Usage: $0 < mtz_file > < chain ID >"
  exit 1
fi

INPUT_MTZ=$1
CHAIN_ID=$2

echo -e "03_refmac/${INPUT_MTZ}\t03_refmac/${INPUT_MTZ}\t${CHAIN_ID}"
