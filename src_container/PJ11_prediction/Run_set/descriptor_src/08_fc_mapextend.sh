#!/bin/bash

source /usr/local/ccp4/bin/ccp4.setup-sh

if [ $# -lt 3 ];then
	echo "args[0]: fc file directory"
	echo "args[1]: output directory"
	echo "args[2]: file name  prefix"
	exit
fi

fc_dir=$1
fullmap_dir=$2
prefix=$3

if [ ! -e ${fullmap_dir}/${prefix} ];then
	mkdir -p ${fullmap_dir}/${prefix}
fi

for file in `ls -1 ${fc_dir}/${prefix}/*.map`
do
# change the extent of MAPIN to match that of XYZIN    
mapmask MAPIN ${file}    \
MAPOUT ${fullmap_dir}/${prefix}/`basename ${file} .map`_fc_cell.map <<EOF
xyzlim CELL     
EOF

done
