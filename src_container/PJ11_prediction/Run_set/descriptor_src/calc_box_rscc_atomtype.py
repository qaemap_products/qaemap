import argparse
import collections
import csv
import fileinput
import glob
import itertools
import json
import logging
import os.path
import random
import re
import sys
import shutil
from pathlib import Path
import mrcfile
import math

import numpy as np
from argparse import RawTextHelpFormatter
from mrcfile.mrcinterpreter import MrcInterpreter

AMINO_BOX_SIZE = {"PHE": 24, "LEU": 24, "ILE": 24, "MET": 24, "VAL": 24,
                  "SER": 24, "PRO": 24, "THR": 24, "ALA": 24, "TYR": 24,
                  "HIS": 24, "GLN": 24, "ASN": 24, "LYS": 24, "ASP": 24,
                  "GLU": 24, "CYS": 24, "TRP": 24, "ARG": 24, "GLY": 24,
                  "HOH": 24, "ANY": 24}


def resize(array, resn):
    """配列の大きさを変更する関数

    - 配列の中心を切り取るようにサイズを変更する。

    Args:
        array (list): サイズを変更する配列
        resn (str): 残基名

    Returns:
        list: 配列サイズを変更した配列
    """
    size = None
    if(resn in AMINO_BOX_SIZE.keys()):
        size = AMINO_BOX_SIZE[resn]
    else:
        size = AMINO_BOX_SIZE['ANY']

    x = array.shape[0]
    y = array.shape[1]
    z = array.shape[2]
    assert size <= x and size <= y and size <= z,\
           "Size before change: {}\nSize after change: {}".format(array.shape, size)

    resized_array = array[int(x / 2 - size / 2):int(x / 2 + size / 2),
                          int(y / 2 - size / 2):int(y / 2 + size / 2),
                          int(z / 2 - size / 2):int(z / 2 + size / 2)]
    return resized_array


def save_resize_data(file_path, data, resn):
    output_dir = os.path.join(os.path.dirname(file_path), 'resize_map')
    output_file = os.path.join(output_dir, os.path.basename(file_path))

    os.makedirs(output_dir, exist_ok=True)
    shutil.copyfile(file_path, output_file)

    size = data.shape

    mrc = mrcfile.open(output_file, 'r+')
    origin_size = mrc.data.shape
    grid = mrc.header.mx.flatten()[0]
    mrc.set_data(data)
    mrc.update_header_from_data()
    mrc.header.nxstart += int(origin_size[2] / 2 - size[2] / 2)
    mrc.header.nystart += int(origin_size[1] / 2 - size[1] / 2)
    mrc.header.nzstart += int(origin_size[0] / 2 - size[0] / 2)
    mrc.header.mx = grid
    mrc.header.my = grid
    mrc.header.mz = grid
    mrc.close()


def load_data(file_path):
    data = []
    # print("in load_data-> {}".format(file_path))
    try:
#        mrc = MrcInterpreter(open(file_path, 'rb'))
        mrc = MrcInterpreter(open(str(file_path), 'rb'))
    except IOError as err:
        data.append(np.zeros([16, 16, 16]))
    else:
        data.append( mrc.data )
    return mrc.data


def weighting_data_by_divide(data, divid_num: int = 3, coe: float = 1, coe_inc: float = 1):
    """Boxの分割数に応じて重みづけを行う関数

    Args:
        data (list): マップ値を格納した3次元配列（各次元の次元数が一致）
        divid_num (int, optional): Boxの分割数. Defaults to 3.
        coe (float, optional): 係数値. Defaults to 1.
        coe_inc (float, optional): 係数増値. Defaults to 1.

    """
    assert data.ndim == 3, data.ndim
    size = data.shape[0]
    assert divid_num < size / 2,\
           "The number of grids {} cannot be divided into {}.".format(size, divid_num)

    cof = np.zeros(data.shape, dtype=np.float32)

    for layer in range(1, divid_num):
        start = int(size / 2 - layer * math.floor(size / 2 / divid_num))
        end = int(size / 2 + layer * math.floor(size / 2 / divid_num))

        cof[start:end,
            start:end,
            start:end] += 1

    cof[:, :, :] *= coe_inc
    cof[:, :, :] += coe

    return data * cof


def weighting_data_by_length(data, grid_width: float = 0.5, distance: int = 4,
                             coe: float = 1, coe_inc: float = 1):
    """Boxの中心からの距離に対してデータを重みづけする関数

    Args:
        data (list): マップ値を格納した3次元配列（各次元の次元数が一致）
        grid_width (float, optional): グリッド幅[Å/grid]. Defaults to 0.5 Å.
        distance (int, optional): マップからの単位距離[Å]. Defaults to 4 Å.
        coe (int, optional): 係数値. Defaults to 1.
        coe_inc (int, optional): 係数増値. Defaults to 1.

    """
    assert data.ndim == 3, data.ndim
    size = data.shape[0]
    assert distance <= size * grid_width / 2,\
           "Distance {}A from the center exceeds the Box area {}A.".format(
               distance, size * grid_width / 2)

    cof = np.zeros(data.shape, dtype=np.float32)

    # 配列の刻み幅
    step_grid = math.ceil(distance / grid_width)

    step = 1
    while step * step_grid < size / 2:
        start = int(size / 2 - step * step_grid)
        end = int(size / 2 + step * step_grid)
        cof[start:end,
            start:end,
            start:end] += 1
        step += 1

    cof[:, :, :] *= coe_inc
    cof[:, :, :] += coe

    return data * cof


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='calculate RSCC',
        formatter_class=RawTextHelpFormatter
    )
    parser.add_argument(
        'fofc',
        help='2fofc file',
        type=str
    )
    parser.add_argument(
        'fc_dir',
        help='directory of Fc file',
        type=str
    )
    parser.add_argument(
        'fc_suffix',
        help='suffix of Fc file',
        type=str
    )
    parser.add_argument(
        'resn',
        help='residue name',
        type=str
    )
    parser.add_argument(
        '--resize_map', '-o',
        help='Output of intermediate file(Map file after resizing).',
        action="store_true"
    )
    parser.add_argument(
        '--weight_method', '-w',
        choices=["DISTANCE", "GRID"],
        help="Choosing how to weight the map."
    )
    args = parser.parse_args()

    # get Fc files
    pt = Path(args.fc_dir)
    files = pt.glob("*" + args.fc_suffix)
    sum_fc = []
    for fc in list(files):
        # print(fc)
        data_fc = resize(load_data(fc), args.resn)
        # 重みづけ
        if(args.weight_method == 'DISTANCE'):
            data_fc = weighting_data_by_length(data=data_fc,
                                               grid_width=int(os.environ['CELL']) / int(os.environ['GRID']),
                                               distance=int(os.environ['MAP_DIST']),
                                               coe=int(os.environ['MAP_WEIGHT']),
                                               coe_inc=int(os.environ['MAP_WEIGHT_INC']))
        elif(args.weight_method == 'GRID'):
            data_fc = weighting_data_by_divide(data=data_fc,
                                               divid_num=int(os.environ['GRID_DIV_NUM']),
                                               coe=int(os.environ['MAP_WEIGHT']),
                                               coe_inc=int(os.environ['MAP_WEIGHT_INC']))

        if(args.resize_map):
            save_resize_data(fc, data_fc, args.resn)

        if len(sum_fc) < 2:
            sum_fc = data_fc.flatten()
        else:
            sum_fc = sum_fc + data_fc.flatten()

    data_2fofc = resize(load_data(args.fofc), args.resn)
    if(args.weight_method == 'DISTANCE'):
        data_2fofc = weighting_data_by_length(data=data_2fofc,
                                              grid_width=int(os.environ['CELL']) / int(os.environ['GRID']),
                                              distance=int(os.environ['MAP_DIST']),
                                              coe=int(os.environ['MAP_WEIGHT']),
                                              coe_inc=int(os.environ['MAP_WEIGHT_INC']))
    elif(args.weight_method == 'GRID'):
        data_2fofc = weighting_data_by_divide(data=data_2fofc,
                                              divid_num=int(os.environ['GRID_DIV_NUM']),
                                              coe=int(os.environ['MAP_WEIGHT']),
                                              coe_inc=int(os.environ['MAP_WEIGHT_INC']))

    if(args.resize_map):
        save_resize_data(args.fofc, data_2fofc, args.resn)
    cor = np.corrcoef(sum_fc.flatten(), data_2fofc.flatten())[0, 1]
    print(args.fofc, "\t", args.fc_dir, "\t", args.fc_suffix, "\t", cor)
