#!/bin/bash

set -x

if [ $# -lt 4 ];then
    echo "args[0]: rotation number (ex. 0)"
	echo "args[1]: input target mtz file of refmac (ex. 03_refmac/1LC0_A_1GCU_A_B99990007_2.4.mtz)"
    echo "args[2]: input high resolution mtz file of refmac (ex. 03_refmac/1LC0_A_target_1.2.mtz)"
    echo "args[3]: chain id (ex. A)"
	exit
fi

#SCRIPT_DIR=/scripts/PJ11_prediction_ver0.06/Run_set
SCRIPT_DIR=$(dirname "$(cd $(dirname $0); pwd)")

#rotnum=5
rotnum=$1
in_target_mtz=$2
in_high_mtz=$3
chain=$4
out_2fofc="18_rot_2fofc_map_per_res"
out_fc="19_fcmapextend_per_res"
out_2fofcac="11_rot_ac_2fofc_map_per_res"
in_2fofc_dir="04_2fofc_map"
in_fc_dir="06_split_cons_atom_type_multipdb"
tmp_rot_refmac="17_rot_refmac"
tmp_fc="07_fc_map"
tmp_fullmap="08_fc_mapextend"

if [ ! -e ${tmp_rot_refmac} ];then
	mkdir ${tmp_rot_refmac}
fi

if [ ! -e ${out_2fofc} ];then
	mkdir ${out_2fofc}
fi

if [ ! -e ${out_fc} ];then
	mkdir ${out_fc}
fi

if [ ! -e ${out_2fofcac} ];then
    mkdir ${out_2fofcac}
fi

if [ ! -e ${tmp_fc} ];then
	mkdir ${tmp_fc}
fi

if [ ! -e ${tmp_fullmap} ];then
	mkdir ${tmp_fullmap}
fi

prefix_target=`basename ${in_target_mtz} .mtz`
prefix_high=`basename ${in_high_mtz} .mtz`

$SCRIPT_DIR/descriptor_src/07_fc_map.sh ${in_fc_dir} ${tmp_fc} ${prefix_target}
$SCRIPT_DIR/descriptor_src/08_fc_mapextend.sh ${tmp_fc} ${tmp_fullmap} ${prefix_target}

source /usr/local/ccp4/bin/ccp4.setup-sh

#argv[0]:(ex. 03_refmac/1LC0_A_1GCU_A_B99990007_2.4.pdb)
in_refmac_pdb_file=`echo ${in_target_mtz}|sed -e s/mtz\$/pdb/`
#argv[1]:(ex. 17_rot_refmac/1LC0_A_1GCU_A_B99990007_2.4)
out_rot_fc_dir=${tmp_rot_refmac}/${prefix_target}
#argv[2]:(ex. 04_2fofc_map/1LC0_A_1GCU_A_B99990007_2.4_2fofc.map)
in_target_2fofc_file=${in_2fofc_dir}/${prefix_target}_2fofc.map
#argv[3]:(ex. 18_rot_2fofc_map_per_res/1LC0_A_1GCU_A_B99990007_2.4)
out_2fofc_per_res_dir=${out_2fofc}/${prefix_target}
#argv[4]:(ex. 08_fc_mapextend/1LC0_A_1GCU_A_B99990007_2.4)
prefix_in_fc_map_file=${tmp_fullmap}/${prefix_target}
#arbv[5]:(ex. 19_fcmapextend_per_res/1LC0_A_1GCU_A_B99990007_2.4`
out_fc_map_per_res_dir=${out_fc}/${prefix_target}
#argv[7]:(ex. 04_2fofc_map/1LC0_A_target_1.2_2fofc.map)
in_high_2fofc_map=${in_2fofc_dir}/${prefix_high}_2fofc.map
#argv[8]:(ex. 11_rot_ac_2fofc_map_per_res/1LC0_A_1GCU_A_B99990007_2.4)
out_high_2fofc_per_res_dir=${out_2fofcac}/${prefix_target}

$SCRIPT_DIR/descriptor_src/3map_and_pdb_rot_by_ccp4.pl ${in_refmac_pdb_file} ${out_rot_fc_dir} ${in_target_2fofc_file} ${out_2fofc_per_res_dir} ${prefix_in_fc_map_file} ${out_fc_map_per_res_dir} ${rotnum} ${in_high_2fofc_map} ${out_high_2fofc_per_res_dir} ${chain}
