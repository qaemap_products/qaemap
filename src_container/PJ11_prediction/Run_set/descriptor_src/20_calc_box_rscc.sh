#!/bin/bash

if [ $# -lt 1 ];then
    echo "20_calc_box_rscc.sh [mtz file list]"
    echo ""
    echo "mtz file list is tab delimited format like below..."
    echo ""
    echo -e "[target mtz]\t[experimentar mtz] [chain]"
    echo "03_refmac/1LC0_A_1GCU_A_B99990007_2.4.mtz   03_refmac/1LC0_A_target_1.2.mtz [chain] [chain]"
    echo ""
    echo ""
    exit
fi

fofc_dir="11_rot_ac_2fofc_map_per_res"
fc_dir="19_fcmapextend_per_res"
out_dir="10_boxRSCC/"

if [ ! -e ${out_dir} ];then
    mkdir ${out_dir}
fi

while read line
do

    sp=(`echo ${line}`)

    #get prefix of each files
    prefix_tmap=`basename ${sp[0]} .mtz`
    prefix_emap=`basename ${sp[1]} .mtz`
    echo "calc target_map:${prefix_tmap} high_resolution_map:${prefix_emap}"

    #get residue number
    pdb_file=`echo ${sp[0]}|sed s/mtz$/pdb/`
#    start=`cat ${pdb_file}|grep ^ATOM|head -1|cut -c23-26|sed s/" "//g`
#    end=`cat ${pdb_file}|grep ^ATOM|tail -1|cut -c23-26|sed s/" "//g`
    rstart=""
    rend=0

    #get residue info
    declare -A resmap
    while read info
    do
        if [[ ! "${info}" =~ ^(ATOM|HETATM) ]];then
            continue
        fi
        # if [[ "${info}" =~ ^ATOM && ! "${info}" =~ CA ]];then
        #     continue
        # fi
        chain=`echo "${info}"|cut -c 22`
        data=`echo "${info}"|cut -c 18-22`
        num=`echo "${info}"|cut -c 23-26|sed s/" "//g`
        
        if [ "${chain}" !=  "${sp[2]}" ];then
            continue
        elif [ -n "${resmap[${num}]}" ];then
            continue
        fi
        
        if [ "${rstart}" = "" ];then
            rstart=$num
        fi

        if [ "${rend}" -lt "${num}" ];then
            rend=${num}
        fi
        resmap[${num}]=${data}
    done<${pdb_file}


    #initialize output file
    echo "res_num res_name chainID RMSD" > ${out_dir}/${prefix_tmap}_si.txt
    #make file path
    for resnum in `seq ${rstart} ${rend}`
    do   
        fofc_file=`echo "${fofc_dir}/${prefix_tmap}/2fofc_rot0_${resnum}.map"`
        fc_each_dir=`echo "${fc_dir}"/${prefix_tmap}/`
        suffix_fc=`echo "_fc_cell_rot0_${resnum}.map"`
        if [ -e ${fofc_file} ];then
            resn=`grep ^${resnum}$'\t' tmp_exe_shell/exe_${prefix_tmap}_rot0_resn.log | cut -f 2`
            echo "${PJ11_PYTHON} calc_box_rscc_atomtype.py ${fofc_file} ${fc_each_dir} ${suffix_fc}"
            result=(`python3 ${SIF_SCRIPTS_FOLDA}/Run_set/descriptor_src/calc_box_rscc_atomtype.py ${fofc_file} ${fc_each_dir} ${suffix_fc} ${resn} -o -w DISTANCE`)
            echo "${resnum} ${resmap[${resnum}]} ${result[3]}" >> ${out_dir}/${prefix_tmap}_si.txt
        fi
    done
done<$1

