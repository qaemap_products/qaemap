Bootstrap: localimage
From: tensorflow.sif
Stage: devel


%post
    export DEBIAN_FRONTEND=noninteractive
    apt update
    apt install -y wget \
                   libxrender-dev \
                   libfontconfig1 \
                   libxtst6 \
                   openssl \
                   libssl-dev \
                   libfftw3-3 \
                   libfftw3-dev

    # CMake
    wget https://github.com/Kitware/CMake/releases/download/v3.17.1/cmake-3.17.1.tar.gz
    tar zxvf cmake-3.17.1.tar.gz
    cd cmake-3.17.1
    ./bootstrap --prefix=/opt/cmake
    make
    make install
    ln -s /opt/cmake/bin/cmake /usr/bin/cmake

    # CCP4
    wget http://devtools.fg.oisin.rc-harwell.ac.uk/nightly/7.0/ccp4-7.0-linux64-2020-03-18-1720.tar.bz2 -P /usr/local/src
    bunzip2 /usr/local/src/ccp4-7.0-linux64-2020-03-18-1720.tar.bz2
    mkdir /usr/local/ccp4
    tar -xf /usr/local/src/ccp4-7.0-linux64-2020-03-18-1720.tar -C /usr/local/ccp4 --strip-components=1

    # MGLTools
    wget http://mgltools.scripps.edu/downloads/downloads/tars/releases/REL1.5.6/mgltools_x86_64Linux2_1.5.6.tar.gz -P /usr/local/src/


Bootstrap: localimage
From: tensorflow.sif
Stage: final

%files from devel
    /usr/local/ccp4 /usr/local
    /usr/local/src/mgltools_x86_64Linux2_1.5.6.tar.gz /usr/local/src

%post
    export DEBIAN_FRONTEND=noninteractive

    # R, OpenBabel, pdb2pqr, PyMOL
    apt update
    apt install -y libxrender-dev \
                   libfontconfig1 \
                   libxtst6 \
                   openssl \
                   libssl-dev \
                   libfftw3-3 \
                   libfftw3-dev \
                   r-base \
                   openbabel \
                   pdb2pqr \
                   pymol
    apt clean

    # MDTraj
    pip install mdtraj==1.9.4

    # CCP4
    bash /usr/local/ccp4/BINARY.setup --run-from-script

    # MGLTools
    mkdir /usr/local/MGLTools-1.5.6
    tar -zxvf /usr/local/src/mgltools_x86_64Linux2_1.5.6.tar.gz -C /usr/local/src
    cd /usr/local/src/mgltools_x86_64Linux2_1.5.6
    ./install.sh -d /usr/local/MGLTools-1.5.6

    # Set alias
    cat <<EOF >> /etc/bashrc

    source /usr/local/src/mgltools_x86_64Linux2_1.5.6/initMGLtools.sh
EOF

    # remove commercial license softwares
    rm -rf /usr/local/MGLTools-1.5.6/MGLToolsPckgs/mslib
    rm -rf /usr/local/MGLTools-1.5.6/MGLToolsPckgs/UTpackages


%environment
    # CCP4
    export PATH=/usr/local/ccp4/bin:${PATH}

    # MGLTools
    export PATH=/usr/local/MGLTools-1.5.6/bin:${PATH}
