#!/bin/bash
DIR=$(cd $(dirname $0); pwd)

set -euCPo pipefail

sudo singularity build tensorflow.sif tensorflow.def
sudo singularity build base.sif base.def
rm -rf tensorflow.sif

cd ../ && tar zcf PJ11_prediction.tar.gz src_container
mv PJ11_prediction.tar.gz ./build/ && cd ${DIR}
sudo singularity build qaemap.sif qaemap.def && mv qaemap.sif ../
rm -rf base.sif PJ11_prediction.tar.gz logs.tar.gz
cd ../PJ11_prediction && find . -type f -name '*.sh' -exec chmod 555 \{\} \;
