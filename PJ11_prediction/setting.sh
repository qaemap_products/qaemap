DIR=$(cd $(dirname $0); pwd)
export RUNSET=$DIR"/Run_set"
export CONTAINER=$DIR"/../qaemap.sif"
export SIF_SCRIPTS_FOLDA="/PJ11_prediction"
export TRAINED_MODEL="/PJ11_prediction/logs"

## CCP4
export PATH=/usr/local/ccp4/bin/:${PATH}
export CELL=100
export GRID=200

export MAP_DIST=4
export MAP_WEIGHT=1
export MAP_WEIGHT_INC=1
export GRID_DIV_NUM=2

# CHANNEL
export HOH_CHANNEL="O"
export UNKNOWN_CHANNEL="X"