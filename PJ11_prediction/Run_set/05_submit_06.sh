#!/bin/sh

if [ $# -lt 1 ];then
    echo "05_submit_06.sh [mtz file list]"
    echo ""
    echo "mtz file list is tab delimited format like below..."
    echo ""
    echo -e "[target mtz]\t[experimentar mtz]\t[chain]"
    echo "03_refmac/1LC0_A_1GCU_A_B99990007_2.4.mtz   03_refmac/1LC0_A_target_1.2.mtz A"
    echo ""
    echo ""
    exit
fi

#pdbdir=$1
mtzlist=$1
outdir="06_split_cons_atom_type_multipdb"

if [ ! -e ${outdir} ];then
        echo "${outdir} directory not found."
        echo "${outdir} is made."
        mkdir ${outdir}
fi

#for pdbfile in `ls -1 ${pdbdir}/*.pdb`
while read line
do
    sp=(`echo ${line}`)
    prefix=`basename ${sp[0]} .mtz`
    pdbfile=`echo ${sp[0]}|sed s/mtz\$/pdb/`
	echo ${pdbfile}
    ${RUNSET}/qsub/step_06_split_cons_atom_type_multipdb-qsub.sh ${pdbfile} ${outdir} &
done<$mtzlist
wait