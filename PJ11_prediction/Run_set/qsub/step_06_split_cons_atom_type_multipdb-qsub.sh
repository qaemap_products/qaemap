#!/bin/bash

PDBFILE=$1
OUTDIR=$2

singularity exec --writable-tmpfs ${CONTAINER} ${SIF_SCRIPTS_FOLDA}/Run_set/descriptor_src/06_split_cons_atom_type_multipdb.sh $PDBFILE $OUTDIR
