#!/bin/bash

ROTNUM=$1
IN_OBJMTZ_FILE=$2
IN_ACCMTZ_FILE=$3
CHAIN=$4
singularity exec --writable-tmpfs ${CONTAINER} ${SIF_SCRIPTS_FOLDA}/Run_set/descriptor_src/rot_and_split_for_each_res_per_PDB.sh ${ROTNUM} ${IN_OBJMTZ_FILE} ${IN_ACCMTZ_FILE} ${CHAIN}
