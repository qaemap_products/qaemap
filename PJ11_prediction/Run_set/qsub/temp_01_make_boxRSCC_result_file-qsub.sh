#!/bin/bash
#$-cwd

set -Ceuo pipefail

singularity exec --writable-tmpfs ${CONTAINER} ${SIF_SCRIPTS_FOLDA}/Run_set/predict_src/01_make_boxRSCC_result_file.sh $1 $2 $3 $4 $5
