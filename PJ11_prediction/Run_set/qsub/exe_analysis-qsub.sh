#!/bin/bash
#$-cwd
set -Ceuo pipefail


LOG_EXE_TEST=$1
FILE_IN=$2
FILE_OUT1=$3
FILE_OUT2=$4
FILE_OUT3=$5

singularity exec --writable-tmpfs ${CONTAINER} Rscript ${SIF_SCRIPTS_FOLDA}/Run_set/predict_src/plot_and_calc_cor_allAA_no-reso.R $LOG_EXE_TEST $FILE_IN $FILE_OUT1
singularity exec --writable-tmpfs ${CONTAINER} Rscript ${SIF_SCRIPTS_FOLDA}/Run_set/predict_src/make_colval_table_for_multi.R $LOG_EXE_TEST $FILE_IN $FILE_OUT2
singularity exec --writable-tmpfs ${CONTAINER} Rscript ${SIF_SCRIPTS_FOLDA}/Run_set/predict_src/merge_all_data_5evaluate_score.R $LOG_EXE_TEST $FILE_IN $FILE_OUT3

