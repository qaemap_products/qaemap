#!/bin/bash
#$ -cwd
set -Ceuo pipefail

INPUT_MTZ=$1
CHAIN_ID=$2

singularity exec --writable-tmpfs --nv ${CONTAINER} ${SIF_SCRIPTS_FOLDA}/Run_set/descriptor_src/make_exe_list_predict.sh $INPUT_MTZ $CHAIN_ID > EXE_LIST.list
singularity exec --writable-tmpfs --nv ${CONTAINER} ${SIF_SCRIPTS_FOLDA}/Run_set/descriptor_src/04_2fofc_map.sh EXE_LIST.list >& log_04
