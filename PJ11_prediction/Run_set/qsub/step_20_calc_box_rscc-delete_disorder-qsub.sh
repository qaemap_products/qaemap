#!/bin/bash
#$ -cwd
set -Ceuo pipefail

LIST=$1
FILE_IN=$2
FILE_OUT=$3

singularity exec --writable-tmpfs ${CONTAINER} ${SIF_SCRIPTS_FOLDA}/Run_set/descriptor_src/20_calc_box_rscc.sh $LIST
singularity exec --writable-tmpfs ${CONTAINER} python3 ${SIF_SCRIPTS_FOLDA}/Run_set/descriptor_src/delete_disorder.py $FILE_IN > $FILE_OUT
