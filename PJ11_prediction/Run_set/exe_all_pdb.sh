#!/bin/bash


if [ $# -lt 2 ];then
    echo ""
	echo "Usage: ./exe_all_pdb.sh [mtz file list] [rotnum]"
    echo ""
	exit
fi

mtz_list=$1
shell_dir="tmp_exe_shell"

if [ ! -e ${shell_dir} ];then
	mkdir ${shell_dir}
fi
eo_path=`readlink -f ${shell_dir}`

#rotnum=0
#list=$1
for rotnum in $*
do
    expr ${rotnum} + 1 >&/dev/null
    if [ $? -gt 1 ];then
        continue
    fi

#	for in_refmac_file in `ls -1 03_refmac/*.pdb`
	while read line
	do
        sp=(`echo ${line}`)
        if [ ${#sp[@]} -lt 3 ];then
            continue
        fi
        in_objmtz_file=${sp[0]}
        in_accmtz_file=${sp[1]}
        chain=${sp[2]}

		prefix=`basename ${in_objmtz_file} .mtz`
		shell_name=`echo ${shell_dir}/exe_${prefix}_rot${rotnum}.sh`
		if [ -e ${shell_name} ];then
			continue
		fi

		echo "#!/bin/bash" > ${shell_name}
		echo "${RUNSET}/qsub/rot_and_split_for_each_res_per_PDB-qsub.sh ${rotnum} ${in_objmtz_file} ${in_accmtz_file} ${chain}" >> ${shell_name}
		bash ${shell_name} > ${eo_path}/exe_${prefix}_rot${rotnum}.log 2> ${eo_path}/exe_${prefix}_rot${rotnum}_resn.log &
    done<${mtz_list}
	wait
#	done<${list}

done
