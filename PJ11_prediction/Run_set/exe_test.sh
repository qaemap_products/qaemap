#!/usr/bin/bash


if [ $# -ne 1 ]; then
  echo "Usage: $0 < TRAINED_MODEL >"
  exit 1
fi
TRAINED_MODEL=$1

#declare -a model_list=("kdeep" "squeezenet" "lenet5like")
declare -a model_list=("squeezenet")
#declare -a obj_list=("rmsd" "boxrscc" "rscc")
#declare -a obj_list=("boxrscc" "rscc")
declare -a obj_list=("boxrscc")
declare -a aa_list=("ALA" "ARG" "ASN" "ASP" "CYS" "GLN" "GLU" "GLY" "HIS" "ILE" "LEU" "LYS" "MET" "PHE" "PRO" "SER" "THR" "TRP" "TYR" "VAL")
declare -a win_list=("12")


suffix=`date "+%Y%m%d_%H%M%S"`
job_num=1
echo -n > log_exe_test_${suffix}
for obj_val in ${obj_list[@]};
do
    for model in ${model_list[@]};
    do
        for aa in ${aa_list[@]};
        do
                for size in ${win_list[@]};
                do
                    ${RUNSET}/test_40epoch.sh ${aa} ${model} ${obj_val} ${size} ${CONTAINER} ${SIF_SCRIPTS_FOLDA} ${TRAINED_MODEL} ${RUNSET} > job${job_num}.o${job_num} 2> job${job_num}.e${job_num}

                    echo -e "${job_num} ${aa} ${model} ${obj_val} ${size}" >> log_exe_test_${suffix}
                    job_num=`expr ${job_num} + 1`
                done
        done
    done
done
