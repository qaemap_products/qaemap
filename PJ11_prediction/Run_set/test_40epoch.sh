#!/bin/bash

#$-cwd
#$-l GPU=1
##$-t 1-10:1
#set -eux

set -Ceuo pipefail

##setopt EXTENDED_GLOB


#nvidia-modprobe -u -c=0
if [ $# -lt 1 ];then
    echo "args[0]: amino acid name using 3 letter ( ILE, TYR,,,)"
    echo "args[1]: model name (kdeep, squeezenet, lenet5like)"
    echo "args[2]: objective valiable ( boxrscc, rscc, rmsd )"
    echo "args[3]: box size"
    echo "args[4]: container"
    echo "args[5]: scripts folda"
    echo "args[6]: TRAINED_MODEL"
    echo "args[7]: RUNSET"
    exit
fi


aa_name=$1
model_name=$2
obj_val=$3
box_size=$4
CONTAINER_PATH=$5
SCRIPTS_FOLDA=$6
TRAINED_MODEL=$7
RUNSET=$8
box_grid_num=`echo $(( $4 * 2 ))`

logdir_name=${model_name}_${obj_val}_${aa_name}_${box_size}
inp_name="--input=voxel.input.linc_pj11_regression"
class_num=`echo -n ""`
if [ "${model_name}" = "lenet5like" ];then
    class_num=`echo -n "--model-num-classes=3"`
    if [ "${obj_val}" = "rmsd" ];then
        inp_name="--input=voxel.input.linc_pj11_classification_rmsd"
    else
        inp_name="--input=voxel.input.linc_pj11_classification_rscc"
    fi
fi

obj_val_dir=`echo 10_boxRSCC`
if [ ${obj_val} = "rmsd" ];then
    obj_val_dir="09_residueRMSD"
elif [ ${obj_val} = "rscc" ];then
    obj_val_dir="13_edstats_3DCNN_chain"
fi

obj_suffix="_si.txt"
if [ ${obj_val} = "rscc" ];then
    obj_suffix="_fofc_stats_coor.out"
fi


# kdeep RSCC
singularity exec --writable-tmpfs --nv ${CONTAINER_PATH} /usr/bin/python3 ${SCRIPTS_FOLDA}/Run_set/src/test.py \
--logdir=${TRAINED_MODEL}/${logdir_name} \
--max_steps=$(( $(singularity exec --writable-tmpfs ${CONTAINER_PATH} ${SCRIPTS_FOLDA}/Run_set/predict_src/count_resnum.sh data/set_test ./data/03_refmac ) * 24 )) \
--batch_size=24 \
--model=voxel.models.${model_name} \
--model-num-channels=5 \
${inp_name} \
${class_num} \
--log_level=DEBUG \
--model-width=${box_grid_num} \
--model-height=${box_grid_num} \
--model-depth=${box_grid_num} \
--residue-name=${aa_name} \
--atom-types=C,O,N,S \
--log_level=DEBUG \
$(cat data/set_test | sed --regexp-extended "s%^%data/${obj_val_dir}/%g" | sed --regexp-extended "s%$%${obj_suffix}%g")

