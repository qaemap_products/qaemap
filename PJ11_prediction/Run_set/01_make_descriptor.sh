#!/bin/sh

if [ $# -ne 3 ]; then
  echo "Usage: $0 < DIR > < INPUT_MTZ > < CHAIN_ID >"
  exit 1
fi

DIR=$1
INPUT_MTZ=$2
CHAIN_ID=$3

echo -ne "[........................................]\r"
#echo "step_0"
$RUNSET/qsub/step_04_2fofc_map-qsub.sh $INPUT_MTZ $CHAIN_ID
echo -ne "[#.......................................]\r"

#echo "step_1"
$RUNSET/05_submit_06.sh EXE_LIST.list > 05_qsub.log
echo -ne "[##......................................]\r"

#echo "step_2"
$RUNSET/exe_all_pdb.sh EXE_LIST.list 0 > exe_all.log

###
#echo "step_3"
$RUNSET/qsub/step_20_calc_box_rscc-delete_disorder-qsub.sh EXE_LIST.list ./10_boxRSCC/"${INPUT_MTZ%.*}"_si.txt ./10_boxRSCC/"${INPUT_MTZ%.*}"_si.txt.temp > step_20.log
echo -ne "[###########################.............]\r"

mv ./10_boxRSCC/"${INPUT_MTZ%.*}"_si.txt.temp ./10_boxRSCC/"${INPUT_MTZ%.*}"_si.txt
