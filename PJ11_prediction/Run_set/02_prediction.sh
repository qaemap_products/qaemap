if [ $# -ne 2 ]; then
  echo "Usage: $0 < DIR > < TRAINED_MODEL >"
  exit 1
fi

DIR=$1
TRAINED_MODEL=$2

$RUNSET/exe_test.sh $TRAINED_MODEL
