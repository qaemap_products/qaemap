#!/bin/sh

if [ $# -ne 3 ]; then
  echo "Usage: $0 < absolute path of pdb_file > < absolute path of mtz_file > < chain ID >"
  exit 1
fi

PATH_OF_INPUT_PDB=$1
if [ ! -e $PATH_OF_INPUT_PDB ]; then
  echo "Error : " $PATH_OF_INPUT_PDB "does not exist"
  exit 1
fi

PATH_OF_INPUT_MTZ=$2
if [ ! -e $PATH_OF_INPUT_MTZ ]; then
  echo "Error : " $PATH_OF_INPUT_MTZ "does not exist"
  exit 1
fi

CHAIN_ID=$3

INPUT_PDB=$(basename $PATH_OF_INPUT_PDB)
INPUT_MTZ=$(basename $PATH_OF_INPUT_MTZ)


DIR=$(cd $(dirname $0); pwd)
ROOTFOLDA=${INPUT_PDB%.*}

. $DIR/setting.sh

if [ -e $ROOTFOLDA ]; then
  echo "Error :" $ROOTFOLDA "directory already exists"
  exit 1
fi

###### 00 一時ディレクトリの作成 ##################
mkdir $ROOTFOLDA
mkdir $ROOTFOLDA/data
mkdir $ROOTFOLDA/data/03_refmac
cp $PATH_OF_INPUT_PDB $ROOTFOLDA/data/03_refmac
cp $PATH_OF_INPUT_MTZ $ROOTFOLDA/data/03_refmac

cd $ROOTFOLDA/data

###################################################

###### 01 記述子の作成 ############################
$DIR/Run_set/01_make_descriptor.sh $DIR $INPUT_MTZ $CHAIN_ID

###################################################

###### 02 予測 ####################################
echo ${INPUT_PDB%.*} > set_test
cd ../
mkdir logs
echo -n > ./logs/test.txt
#echo "step_4"
$DIR/Run_set/02_prediction.sh $DIR $TRAINED_MODEL

###################################################

###### 03 解析 ####################################
mkdir io
for log_file in `ls job*.*`;
do
  mv ${log_file} test_40epoch.sh.${log_file#*.}
done

mv test_40epoch.sh.* io
LOG_EXE_TEST=$(ls log_exe_test_*)
#echo "step_5"
$DIR/Run_set/qsub/exe_analysis-qsub.sh $LOG_EXE_TEST io/test_40epoch.sh out_exe_test out_exe_test out_exe_test_difval.dat > exe_analysis-qsub.log

#echo "step_6"
$DIR/Run_set/qsub/temp_01_make_boxRSCC_result_file-qsub.sh out_exe_test_difval.dat ${INPUT_PDB%.*} 12 ./data/03_refmac/$INPUT_PDB ${INPUT_PDB%.*}_output > temp_01_make_boxRSCC_result_file-qsub.log

echo -ne "[########################################]\r"
echo ""
###################################################
