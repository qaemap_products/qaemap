# QAEmap

## Environment
This step requires following software  to be installed on your machine.
- Singularity

## acknowledgement
These softwares comunicate with the following separate libry and the package.
- CCP4
- Tensorflow

## first time setup
The following steps are required in order to run QAEmap

1. Clone this repository and "cd" into "build" directory.

2. Build the singularity image<br>
./build.sh


## Running QAEmap
The steps to run are following procedure.

-  preparing the protein coordinate file (PDB format) and 2fofc map(mtz format）
-  Running the Command run_predict.sh<br>
[QAEmap_TO_PATH]/PJ11_prediction/run_predict.sh [PDB] [MTZ] [CHAIN<br>
[QAEmap_TO_PATH] is the setting directory of QAEmap

## License and Disclaimer
This source is not an officialy support MKI product. 
Copyright 2021 Mitsui Knowledge Industry

### QAEmap Code License

The source code  are made available for non-commercial use only, under the terms of the Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC-ND 4.0) license. You can find details at: <br>
<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License</a>.

### Third-party software
Use of the third-party software, libraries or code referred to in the Enviroment section above may be governed by separate terms and conditions or license provisions. Your use of the third-party software, libraries or code is subject to any such terms and you should check that you can comply with any applicable restrictions or terms and conditions before use.
